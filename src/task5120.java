import java.util.ArrayList;
import java.util.Iterator;

public class task5120 {
    public static void main(String[] args) throws Exception {
        //stask 1: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("Red");
        colors.add("Green");
        colors.add("Blue");
        colors.add("Yellow");
        colors.add("Purple");

        System.out.println(colors);

        //stask 2: Tạo mới 2 ArrayList Integer, thêm 3 số (kiểu int) vào mỗi ArrayList, cộng các giá trị của một ArrayList vào ArrayList còn lại. Ghi ra terminal ArrayList được cộng thêm.
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(4);
        list2.add(5);
        list2.add(6);

        for (int i = 0; i < list1.size(); i++) {
            int sum = list1.get(i) + list2.get(i);
            list1.set(i, sum);
        }
        System.out.println(list1);

        //stask 3: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal số lượng phần tử của ArrayList vừa tạo
        //sử dụng code của subtask 1
        System.out.println("So phan tu mau co trong mang mau: " + colors.size());

        //stask 4: ghi ra terminal phần tử thứ 4 của ArrayList 
        System.out.println("Phan tu mau vi tri thu 4 co trong mang mau: " + colors.get(3));

        //stask 5: ghi ra terminal phần tử cuối cùng của ArrayList vừa tạo (Không sử dụng index trực tiếp)
        System.out.println("Phan tu mau vi tri cuoi cung co trong mang mau: " + colors.get(colors.size() - 1));

        //stask 6: Xóa phần tử cuối cùng của ArrayList trên và ghi lại ra terminal ArrayList vừa tạo.
        colors.remove(colors.size() - 1);
        System.out.println("mang mau sau khi xaa phan tu cuoi cung la: " + colors);

        //stask 7: Sử dụng forEach ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        System.out.println("Su dung forEach ghi ra terminal gia tri tung phan tu của ArrayList vua tao");
        colors.forEach(color -> System.out.println(color));

        //stask 8: Sử dụng iterator ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        Iterator<String> iterator = colors.iterator();
        System.out.println("Su dung iterator ghi ra terminal gia tri tung phan tu của ArrayList vua tao");
        while (iterator.hasNext()) {
            String color = iterator.next();
            System.out.println(color);
        }

        //stask 9: Sử dụng vòng lặp for ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        System.out.println("Su dung for ghi ra terminal gia tri tung phan tu của ArrayList vua tao");
        for (String color : colors) {
            System.out.println(color);
        }

        //stask 10: Thêm một màu sắc vào đầu của ArrayList trên và ghi lại ra terminal ArrayList vừa tạo.
        colors.add(0, "Orange");
        System.out.println("Them mot mau sac vao dau cua ArrayList");
        System.out.println(colors);

        //stask 11: Sửa màu của phần tử thứ 3 của ArrayList trên thành màu vàng và ghi lại ra terminal ArrayList vừa tạo.
        colors.set(2, "Yellow");
        System.out.println("Sua mau cua phan tu thu 3 cua ArrayList");
        System.out.println(colors);

        //stask 12: Tạo mới 1 ArrayList String, thêm lần lượt các giá trị John, Alice, Bob, Steve, John, Steve, Maria vào ArrayList vừa tạo. Ghi ra terminal vị trí đầu tiên của 2 phần tử có giá Alice và Mark trong ArrayList trên.
        ArrayList<String> list = new ArrayList<>();
        list.add("John");
        list.add("Alice");
        list.add("Bob");
        list.add("Steve");
        list.add("John");
        list.add("Steve");
        list.add("Maria");

        int aliceIndex = list.indexOf("Alice");
        int markIndex = list.indexOf("Mark");

        System.out.println("Vi tri đau tien cua Alice: " + aliceIndex);
        System.out.println("Vi tri đau tien cua Mark: " + markIndex);

        //stask 13: Ghi ra terminal vị trí cuối cùng xuất hiện của 2 phần tử có giá Steve và John trong ArrayList trên.
        int steveIndex = list.lastIndexOf("Steve");
        int johnIndex = list.lastIndexOf("John");

        System.out.println("Vi tri cuoi cung xuat hien cua Steve: " + steveIndex);
        System.out.println("Vi tri cuoi cung xuat hien cua John: " + johnIndex);

    }
}
